(* Welcome to the OCaml Starter for Ultimate TicTacToe on theaigames.com
 * Look at go.ml for all the details.
 *)
open Td;;

let main gstate =
  let moves = Go.test_moves gstate Go.is_sensible in
  if List.length moves > 0 then
    let move = Go.random_from_list moves in
      Go.issue_order move
  else Go.pass_turn ()
;;

Go.run_bot main

