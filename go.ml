(* Welcome to the OCaml Starter for Go on theaigames.com
 * Have a look at the utility functions at the end of this file.
 *
 * The debug function below is helpful for writing to stderr or a file.
 * Just remove the comment marks and () from the function below if you want 
 * to use it.
 *
 * If you find any bugs, please notify the
 * developer here:
     https://github.com/smiley1983/go_starter_ocaml
 *)

open Td;;

let get_time () = Unix.gettimeofday ();;

let out_chan = stderr (* open_out "mybot_err.log" *);;

let debug s = (* *)
   output_string out_chan s; 
   flush out_chan
(*
*)
;;

(* input processing *)

let owner gstate i =
  if (i = gstate.setup.your_botid) then `Friend
  else `Enemy
;;

let int_to_cell gstate i =
  match i with
  | 0 -> `Empty
  | -1 -> `Ko
  | n -> owner gstate i
;;

let string_to_cell gstate i =
  match i with
  | "0" -> `Empty
  | "-1" -> `Ko
  | s -> owner gstate (int_of_string s)
;;

let uncomment s =
  try String.sub s 0 (String.index s '#')
  with Not_found -> s
;;

let new_board size =
   Array.make_matrix size size `Empty
;;

let clear_gstate gstate =
 (
  gstate.board <- new_board gstate.setup.field_width;
  gstate.round <- 0;
  gstate.move <- 0;
 )
;;

(* tokenizer from rosetta code *)
let split_char sep str =
  let string_index_from i =
    try Some (String.index_from str i sep)
    with Not_found -> None
  in
  let rec aux i acc = match string_index_from i with
    | Some i' ->
        let w = String.sub str i (i' - i) in
        aux (succ i') (w::acc)
    | None ->
        let w = String.sub str i (String.length str - i) in
        List.rev (w::acc)
  in
  aux 0 []
;;

let string_of_cell = function
  | `Empty -> ". "
  | `Friend -> "o "
  | `Enemy -> "x "
  | `Ko -> "! "
;;

let debug_board board =
  Array.iter (fun row ->
    Array.iter (fun v -> debug (string_of_cell v)) row;
    debug("\n")
  ) board;
  debug("\n\n");
;;

let update_game_board (gstate:game_state) data =
   if Array.length gstate.board < gstate.setup.field_height then (
     gstate.board <- new_board gstate.setup.field_height;
   );
   let size = gstate.setup.field_width in (* * gstate.setup.field_height in *)
   List.iteri (fun i (c:string) ->
      let row = i / size in
      let col = i mod size in
         gstate.board.(row).(col) <- string_to_cell gstate c
   ) (split_char ',' data); (* (Str.split (Str.regexp ",") data) *)
;;

let action_move bot gstate t2 =
  gstate.last_timebank <- (int_of_string t2);
  bot gstate
;;

let four_token (gstate:game_state) key t1 t2 t3 =
   if (t3 = "") || (t2 = "") || (t1 = "") || (key = "")  then (
     debug ("four_token fail: " ^ key ^ " " ^ t1 ^ " " ^ t2 ^ " " ^ t3 ^ "\n")
   ) else (
   match key with
    | "update" ->
      begin match t1 with
       | "game" ->
         begin match t2 with
          | "round" ->
            (* Assume that this coincides approximately with start of turn *)
            gstate.last_update <- get_time();
            gstate.round <- int_of_string t3
          | "move" -> gstate.move <- int_of_string t3
          | "field" -> update_game_board gstate t3
          | _ -> ()
         end
       | player -> 
           begin match t2 with
           | "points" ->
             if gstate.setup.your_bot = player then
               gstate.my_points <- float_of_string t3
             else
               gstate.opponent_points <- float_of_string t3
           | "last_move" -> gstate.last_move <- None
           | _ -> ()
           end
      end
    | _ -> ()
    )
;;

let three_token bot gstate key t1 t2 =
   if (t2 = "") || (t1 = "") || (key = "")  then ( 
     debug ("three_token fail: " ^ key ^ " " ^ t1 ^ " " ^ t2 ^ " " ^ "\n")
   ) else (
     match key with
      | "settings" -> 
        begin match t1 with
         | "timebank" -> gstate.setup.timebank <- int_of_string t2
         | "time_per_move" -> gstate.setup.time_per_move <- int_of_string t2
         | "player_names" -> gstate.setup.player_names <- split_char ',' t2 
         | "your_bot" -> gstate.setup.your_bot <- t2
         | "your_botid" -> gstate.setup.your_botid <- int_of_string t2
         | "field_width" -> gstate.setup.field_width <- int_of_string t2
         | "field_height" -> gstate.setup.field_height<- int_of_string t2
         | _ -> ()
        end
      | "action" -> 
        begin match t1 with
         | "move" -> action_move bot gstate t2
         | _ -> ()
        end
      | _ -> ()
   )
;;

let last_move gstate row col =
  gstate.last_move <- Some (row, col)
;;

let process_line bot gstate line =
  let tokens = split_char ' ' (uncomment line) in
  match tokens with
  | upd :: pl :: lmove :: plmove :: col :: row :: [] -> 
    last_move gstate (int_of_string row) (int_of_string col)
  | t1 :: t2 :: t3 :: t4 :: [] -> four_token gstate t1 t2 t3 t4
  | t1 :: t2 :: t3 :: [] -> three_token bot gstate t1 t2 t3
  | _ -> debug ("Incorrect bot input: " ^ line ^ "\n")
  (*
  match List.length tokens with
  | 4 -> four_token gstate (List.nth tokens 0) (List.nth tokens 1) (List.nth tokens 2) (List.nth tokens 3)
  | 3 -> three_token bot gstate (List.nth tokens 0) (List.nth tokens 1) (List.nth tokens 2)
  | _ -> debug ("Incorrect bot input: " ^ line ^ "\n")
  *)
;;

let read_lines bot gstate =
  while true do
    let line = read_line () in
      process_line bot gstate line;
  done
;;

(* End input section *)

(* output section *)

let issue_order (row, col) =
   Printf.printf "place_move %d %d\n" col row;
   flush stdout;
;;

let pass_turn () = 
  Printf.printf "pass\n";
  flush stdout;
;;

(* End output section *)

(* Utility functions *)

let random_from_list lst =
  let len = List.length lst in
    List.nth lst (Random.int len)
;;

let in_bounds gstate row col =
  row >= 0 && col >= 0 
  && row < gstate.setup.field_height
  && col < gstate.setup.field_width
;;

let get_adjacent gstate row col =
  List.fold_left 
    (fun acc (ofr, ofc) ->
      let nr = row + ofr in
      let nc = col + ofc in
      if in_bounds gstate nr nc then
        (nr, nc) :: acc
      else acc
    ) 
    [] 
    [(-1, 0); (0, 1); (1, 0); (0, -1)]
;;

let rec search_step gstate dfs fillval (row, col) = 
  if not dfs.visited.(row).(col) then (
    dfs.visited.(row).(col) <- true;
    if gstate.board.(row).(col) = fillval then (
      dfs.matched <- (row, col) :: dfs.matched;
      let adjacents = get_adjacent gstate row col in
        List.iter (search_step gstate dfs fillval) adjacents
    ) else
      if not (List.exists 
        (fun x -> x = gstate.board.(row).(col)) 
        dfs.reached 
      ) then
        dfs.reached <- 
          gstate.board.(row).(col) :: dfs.reached
  )
;;

let move_search state dfs player row col =
  let prev_val = state.board.(row).(col) in
    state.board.(row).(col) <- player;
    search_step state dfs player (row, col);
    state.board.(row).(col) <- prev_val
;;

let refresh_dfs state dfs =
  dfs.reached <- [];
  dfs.matched <- [];
  Array.iteri (fun ir row ->
    Array.iteri (fun ic cell ->
      row.(ic) <- false
    ) row
  ) dfs.visited;
;;

let group_captured state dfs (row, col) =
  refresh_dfs state dfs;
  search_step state dfs state.board.(row).(col) (row, col);
  not (List.exists (fun v -> v = `Empty || v = `Ko) dfs.reached)
;;

let is_capture gstate dfs player row col =
  let prev_color = gstate.board.(row).(col) in
  let adjacent = get_adjacent gstate row col in
    gstate.board.(row).(col) <- player;
    let result = 
      List.exists (fun (ar, ac) ->
        (gstate.board.(row).(col) != gstate.board.(ar).(ac))
        && (gstate.board.(ar).(ac) != `Empty) 
        && (gstate.board.(ar).(ac) != `Ko)
        && (group_captured gstate dfs (ar, ac))
      ) adjacent
    in
      gstate.board.(row).(col) <- prev_color;
      (* if result then 
        debug ("is_capture " ^ string_of_int row ^ " " ^ string_of_int col); *)
      result
;;

let not_suicide gstate player row col =
  let dfs = {
    visited = Array.make_matrix gstate.setup.field_height gstate.setup.field_width false;
    reached = [];
    matched = [];
  } in
    move_search gstate dfs player row col;
    if List.exists 
      (fun v -> v = `Empty || v = `Ko) 
      dfs.reached 
    then (
(*      debug ((string_of_int row) ^ " " ^ (string_of_int col) ^ " reaches empty\n" ); *)
      true
    ) else
      is_capture gstate dfs player row col
;;

let not_fill_own_eye gstate player row col =
  List.fold_left (fun acc (ar, ac) ->
    acc || gstate.board.(ar).(ac) != `Friend
  ) false (get_adjacent gstate row col)
;;

let is_legal gstate player row col =
  gstate.board.(row).(col) = `Empty
  && not_suicide gstate player row col
;;

let is_sensible gstate player row col =
  is_legal gstate player row col
  && not_fill_own_eye gstate player row col
;;

let test_moves gstate f =
  let result = ref [] in
  let size = gstate.setup.field_width - 1 in
  for row = 0 to size do
    begin for col = 0 to size do
      if f gstate `Friend row col then result := (row, col) :: !result;
    done end
  done;
  !result
;;

let time_elapsed_this_turn gstate =
  (get_time() -. gstate.last_update) *. 1000.
;;

let time_remaining gstate =
  (float_of_int gstate.last_timebank -. time_elapsed_this_turn gstate)
;;

(* End utility *)

let run_bot bot =
  (* Remove or change the self_init if you want predictable random sequences *)
  Random.self_init (); 
  let game_info =
     {
      timebank = 0;
      time_per_move = 0;
      player_names = [];
      your_bot = "";
      your_botid = -1;
      field_width = 0;
      field_height = 0;
     }
  in
  let game_state =
     {
      board = [|[| |]|];
      round = 0;
      move = 0;
      my_points = 0.0;
      opponent_points = 0.0;
      last_move = None;
      last_update = 0.0;
      last_timebank = 0;
      setup = game_info;
     }
  in
  clear_gstate game_state;

  begin try
   (
     read_lines bot game_state
   )
  with exc ->
   (
    debug (Printf.sprintf
       "Exception in turn %d :\n" game_state.round);
    debug (Printexc.to_string exc);
    raise exc
   )
  end;
;;


