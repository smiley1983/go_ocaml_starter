type cell = [ `Empty | `Friend | `Enemy | `Ko ];;

type game_info =
 {
   mutable timebank : int;
   mutable time_per_move : int;
   mutable player_names : string list;
   mutable your_bot : string;
   mutable your_botid : int;
   mutable field_width : int;
   mutable field_height: int;
 }
;;

type game_state =
 {
   mutable board : cell array array;
   mutable round : int;
   mutable move : int;
   mutable my_points : float;
   mutable opponent_points : float;
   mutable last_move : (int * int) option;
   mutable last_update : float;
   mutable last_timebank : int;
   setup : game_info;
 }
;;

type dfs = 
 {
  visited : bool array array;
  mutable reached : cell list;
  mutable matched : (int * int) list;
 }
;;

